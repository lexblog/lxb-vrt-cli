# Introduction

Welcome to LexBlog's Visual Regression Testing tool. This is a command line based tool that is run using Node.js (runtime platform), Selenium (the primary API for running a web browser), and Applitools (a service for comparing screenshots for visual differences). The goal of this application is to provide a simple way to test many sites in the context of Applitools without the overhead of a complex UI framework. 

While small, there are still some finicky dependencies at play. __Please read the "Environment Notes" and "Installation" sections to begin using this tool.__

# Environment Notes

* Must be running the current LTS of Node.js (check by typing `node --version`)
    * If this fails, then download Node
* Eslint is installed as a dev dependency - this can be turned off, of course, but the application was written with this standard in mind.
    * See the .eslintrc.json file and the [ESLint documentation](https://eslint.org) for more information
* The tests run using a geckodriver binary to proxy requests to a Firefox browser. Keep your Firefox browser up to date! 
* If you use Firefox, when running tests you may find that your non-testing browser loses focus to the Firefox test browser; it's suggested to use Chrome if you are going to continue to work while running a baseline or visual regression test.

# Installation

* Clone the repository to a local directory, navigate to it in a terminal, and run `npm install` - this downloads all of the necessary Node modules for the application
* Install geckodriver
    * First, check and see if you have geckodriver downloaded by opening a terminal and typing `which geckodriver` and, if that command comes back with a directory, type `geckodriver --version`
        * If `which geckodriver` returns nothing or `geckodriver --version` is less than 24.0, then follow the steps below
            * Download geckodriver from https://github.com/mozilla/geckodriver/releases (the most current version should be fine)
            * Unzip/unpackage it so that you have a binary file 
            * Move it to `/usr/local/bin` (replacing the older version of geckodriver if it existed)
            * Type in the `which geckodriver` command again and you should see it as `usr/local/bin/geckodriver`
            * Type in the `geckodriver --version` command again and you should see it as 24.0 or greater
    * Second, take note of the directory where the binary is and enter `echo $PATH`. The directory where the geckdriver is should be listed in the resulting statement echoed to the terminal. 
        * If it is not, add it to your path by typing `export PATH=$PATH:[directory]` where `[directory]` is the directory where the binary is; e.g. `export PATH=$PATH:/usr/local/bin`
* Test that the application has installed and works by, in the cloned directory in a shell session, running the command `node index.js test -n default -t index -f default.json`
    * This will run a short test against a small number of URLs and pass the results to Applitools for review. The results are not important, but the test running, passing screenshots to Applitools, and stopping at the end is the goal of this exercise.

# Usage 

This presumes that you have cloned the repository and followed the installation steps above and have a shell session open where your current working directory is the top-level directory of the cloned respository. 

* Query all of the multisite installations that LexBlog manages using the lxb-rest-multisite-info plugin interface __taking care to check the "Template URLs" checkbox in the "Fields" section of the query__. 
* When the query has completed, a "Download JSON" button should appear; click it and note where the file has downloaded
* Move the file into the `/data/` directory of this application
    * Note that this directory is not versioned, so if you wish to share this file it must be through some other method than through Git
* Note the name of this file; for our purposes we'll call it `test.json` and assume that the test name is `prod-1234` and we are testing the single.php template
* In the shell session, type the command `node index.js test -n prod-1234 -t single -f test.json`
* This will open a Firefox browser and set a baseline for a visual regression test on the URLs contained in the test.json file
* Open Applitools and ensure that a test with the name `testname` has been created and is taking screenshots of the URLs you desire
* When this has completed, make any modifications to the codebase that is under test
* When done re-enter the command `node index.js test -n prod-1234 -t single -f test.json` and review Applitools for issues that arise 

The CLI supports the following commands, all of which can be seen by typing `node index.js test --help`:

* `--name`, `-n` (required)
    * The name of the test in Applitools. This must be unique and needs to be identical between the baseline and regression runs of the CLI. 
* `--template`, `-t` (required)
    * The name of the template under test. Options include:
        * index
        * single
        * user_archive
        * category_archive
* `--filename`, `-f` (optional)
    * The name of the file in the /data/ directory to use
    * Defaults to `data.json` if no name is provided
* `--mobile`, `-m` (optional)
    * If `y` is passed, then the browser will have a width of 500px, thus triggering most of the mobile styles
    * Otherwise the browser will default to a width of 1200px

# Roadmap

As LexBlog continues to build out and work on this application, here are some future nice-to-haves for development:

* Provide better browser support
    * The package.json file contains the `chromedriver` package for this purpose - to switch to using this browser, simply update any instance of: 

```javascript
let driver = new Builder()
.forBrowser('firefox')
.build();
```

To 

```javascript
let driver = new Builder()
.forBrowser('chrome')
.build();
```

* Support command line arguments for multiple browsers running in parallel (currently this has just two processes - one boss, one worker, but the modules inside the `/commands/` directory could spawn multiple processes and divy up the work).
* Multi-template/different test flow support
    * The only test in this tool is `/tests/api-run.js` which visits a URL and takes a screenshot.
    * To augment this or add a new test: 
        * Update the `/commands/api-run.js` file to accept an argument for which test to run (currently this test file is hardcoded in the `testPath` variable in the module)
        * Add the new test and have it perform any navigation or additional actions