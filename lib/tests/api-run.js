// Small class for reading results from the JSON file provided by lxb-rest-multisite-info
const DataReader = require( '../utils/datareader' );
// Webdriver packages that we use
const { Builder } = require('selenium-webdriver');
const Promise = require('bluebird');

// Set up Applitools SDK and eyes object that lets us take screenshots and talk to Applitools
let SeleniumSDK = require( 'eyes.selenium' );
let Eyes = SeleniumSDK.Eyes;
let eyes = new Eyes();
eyes.setApiKey( '4H3AJr7Em6VlDxmjuQYrgH25U0BwnRXzMi9g9xKnZH4110' );
// Configure the browser to take a full page screenshot with CSS (so that we don't repeat pos fixed elements)
eyes.setForceFullPageScreenshot( true );
eyes.setStitchMode( Eyes.StitchMode.CSS );

// Hide the scrollbars in the browser
eyes.setHideScrollbars( true );
// Set the browser width
let browserWidth;
// If the user has declared this to be a mobile test
if( process.argv[5] != 'n' ){
    // Set a browser width indicative of a mobile viewport
    browserWidth = { width: 500, height: 700 };
} else {
    // Otherwise give it a desktop width
    browserWidth = { width: 1200, height: 700 };
}
// Set the batch name using the test name passed into the process
let testName = process.argv[2];

eyes.setBatch( testName, testName );

// Get the URLs that we want by instantiating the DataReader class
const dataReader = new DataReader();
// And then loading from the default or user-defined JSON file
if( process.argv[4] != 0 ){
    dataReader.loadData( process.argv[4] );
} else {
    dataReader.loadData();
}

// Then load the kind of template URLs we're interested in into an array for use
let URLArr = dataReader.getURLs( process.argv[3] );

// Get the driver aware of which browser it should use
let driver = new Builder()
    .forBrowser('firefox')
    .build();

const results = validate(URLArr);

// Go through all URLs and confirm response code is 200
async function validate( urls ) {
    let validResults = [];

    // Add valid URLs to validResults then process that list
    if ( urls.length > 0) {
      console.log('Checking URLs');
      validResults = await dataReader.makeRequests(URLArr);
      processList(validResults);
    }
}

// Run valid URLs through screenshot tool
async function processList(validURLList, promises = []) {
  let remainingUrls = [];
  let allPromises = [];
  if (validURLList.length > 0) {
    console.log("Remaining URLs: ", validURLList.length);

    // Take the screenshot and remove the URL from the list
    try {
      let screenshotResult = await takeScreenshot(validURLList[0].url);
      allPromises = [...promises, screenshotResult];

      remainingUrls = validURLList.slice(1);
    }
    // Once we're done processing URLs
    finally {
        if(validURLList.length == 0) {
          console.log("Done!")
          // Quit the driver
          await driver.quit();
          // Shutdown the Eyes object
          eyes.abortIfNotClosed();
        } else {
          return await processList(remainingUrls, allPromises)
        }
    }
  } else {
    console.log('List of URLs is empty. Please make sure you are providing the correct file.');
    // Quit the driver
    await driver.quit();
    // Shutdown the Eyes object
    eyes.abortIfNotClosed();
  }
}

async function takeScreenshot(url) {
  browserWidth = { width: 1200, height: 700 };
  // Invoke the eyes.open function to prep Applitools
  await eyes.open( driver, 'Platform', url + ' ' + testName, browserWidth );


  // Navigate to the given URL
  await driver.get( url );
  // Take a screenshot
  await eyes.checkWindow(); 
  // And close the test without throwing an exception if there is a mismatch
  // so that we can continue along our merry way
  return await eyes.close( false );
}
