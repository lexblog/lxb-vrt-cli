'use strict';

// Requre the filesystem and path
const fs = require( 'fs' );
const path = require( 'path' );
const axios = require( 'axios' );
const Promise = require("bluebird");

/** Class for reading a JSON file provided by the lxb-rest-multisite-info plugin */
module.exports = class DataReader {

    /**
     * Set some class-level variables
     */
    constructor(){
        this.data = {};
        this.response = [];

    }

    /**
     * Load the data from a JSON file into a JavaScript object
     * @param {string} fileName - The name of the file to use. Must always be in the /data/ folder and be a file from the lxb-rest-multisite-info plugin.
     */
    loadData( fileName = 'data.json' ){
        // Try to load the file into the data object
        try {
            this.data = JSON.parse(fs.readFileSync( path.join( __dirname, '../../data/' + fileName ), 'utf8' ) );
        }
        // If an error is thrown, try to handle it
        catch(err){
            // Most commonly we're going to have some issues with the filename, so in that case let's just let the user know
            if (err.code === 'ENOENT') {
                console.log('File - ' + fileName + ' -  not found!');
            }
            // Otherwise it could be all sorts of errors and so we'll just throw the stack trace into stderr
            else {
                throw err;
            }
        }
    }

    /**
     * Returns an array of URLs to be used by a test.
     * @param {string} template - The URL type to be used.
     */
    getURLs ( template ) {
        // Hands off the work of constructing the URL array to another function
        this.urlsOfType( template );
        // Once there is data in the response array
        if( this.response.length > 0 ){
            // Return valid URLs
            return this.response;
        }
    }

    /**
     * Queue up all URLs to send off to the asyncRequest function.
     * Response from that function is a list of valid URLs to then send
     * back to the script to run through them
     * Loops through this until all URLs have been processed
     */
    async makeRequests( urls, promises = []) {
        if (urls.length === 0) {
            return promises;
        }

        const result = await this.asyncRequest(urls[0]);
        const allPromises = [...promises, result];

        const remainingUrls = urls.slice(1);

        await this.sleep();

        return await this.makeRequests(remainingUrls, allPromises);
    }

    /**
     * Fire off a request to the give URL.
     * If a 200 is returned, send back a success so it is added to
     * the valid URLs list.
     * If an error is returned, print out the invalid URL
     */
    asyncRequest( url ) {
        // Make request and process response
        return axios.get(url).then((response) => {
            // If a valid URL
            if (response.status == 200) {
                console.log('200: ', url);
                // Return success message and valid URL
                return {
                    success: true,
                    url: url,
                };
            } else if (response.status != 200) {
                console.log(response.status + ' URL: ' + url);
                // If not a valid URL, return false success, bad url, and response status
                return {
                    success: false,
                    url: url,
                    status: response.status,
                };
            }
        }).catch((err) => {
            // If axios returns an error, print out the error
            if (err.errno === 'ENOTFOUND') {
                console.log('Bad URL: ', url);
                console.log('Reason: ', err.errno);

                // Return false success, bad url, response status, and error code
                return {
                    success: false,
                    url: url,
                    status: err.errno,
                    msg: err.code,
                };
            } else {
                console.log('Bad URL: ', url);
                console.log('Reason: ', err.response);
                // Return false success, bad url, response status, and error msg
                return {
                    success: false,
                    url: url,
                    status: err.response,
                    msg: err.response,
                };
            }
        });
    }

    // Wait one second between requests to prevent rate limiting
    sleep() {
        return new Promise((r) => {
            setTimeout(r, 1000);
        });
    }

    /**
     * Stores the URL of a certain type in a class member variable to be returned at some future point
     * @param {*} templateName - What URL type we need to pluck from the JS object that has been built from the JSON file
     */
    urlsOfType( templateName ){

        const maxUrlLength = 91;

        // For each API request
        for( let APIRequest in this.data ){
            // If the request returned data
            if( typeof this.data[APIRequest].length == 'undefined' ){
                // Then for the install in the request
                for( let install in this.data[APIRequest] ){
                    // If the count of sites is greater than 0
                    if( this.data[APIRequest][install].count > 0 ){
                        // Then for each site in the install
                        for( let site in this.data[APIRequest][install].items ){
                            // Add the URL of the template we're interested in to our response array
                            if( this.data[APIRequest][install].items[site].details.templates[templateName] != undefined ){

                                let indexUrl = this.data[APIRequest][install].items[site].details.templates['index'];
                                let indexUrlLength = indexUrl.length;
                                if( indexUrlLength < maxUrlLength ) {
                                    this.response.push( this.data[APIRequest][install].items[site].details.templates[templateName] );
                                }

                            }
                        }
                    }
                }
            }
        }
    }
};

