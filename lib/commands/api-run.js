'use strict';
const { spawn } = require('child_process');
const { join } = require('path');

/** 
 * Command handler for this module. Exposes all of the arguments to a user 
 * and delegates the running of the test to a child process. The structure of the module
 * is a requirement of yargs. 
 */
module.exports = () => {
    let cmd = {};
    // CLI command and description
    cmd.command = 'test';
    cmd.desc = 'Runs the CLI';
    // Command args, aliases, and descriptions
    cmd.builder = {
        name: {
            alias: 'n',
            describe: 'Pass the name of the test',
            demand: true
        },
        template: {
            alias: 't',
            describe: 'Pass the template you wish to test. Should be index, single, user_archive, or category_archive.',
            choices: ['index', 'single', 'user_archive', 'category_archive'],
            demand: true
        }, 
        filename: {
            alias: 'f',
            describe: 'Pass the name of the file to use',
            demand: false
        },
        mobile: {
            alias: 'm',
            describe: 'Using mobile or not; note "y" if you are.',
            demand: false
        }
    };
    // Handles the command once all ags have been probided
    cmd.handler = function (argv) {
        // Save our arguments to be passed to the child node process
        let testPath = join( global.appRoot, 'lib', 'tests', 'api-run.js' );
        let template = argv.template;
        let batchName = argv.name;
        let mobile = 'n';
        if( argv.mobile ){
            mobile = argv.mobile;
        }
        let fileName = 0;
        if( argv.filename ){
            fileName = argv.filename;
        }
        let child;
        // Spawn a child process with the commands passed in
        child = spawn('node', [ testPath, batchName, template, fileName, mobile ] );
        // The spawn() call above spawns a new process which is outside the current process
        // So we need to have a way to communicate with it if there are errors
        child.stderr.on( 'data', ( data ) => {
            console.log( data.toString() );
        });

        child.stdout.pipe(process.stdout);
        // If you find yourself needing to console log things in the child process
        // Uncomment this line out
        /*child.stdout.on( 'data', ( data ) => {
            console.log( data.toString() );
        });*/
    };
    // Return the command to yargs
    return cmd;
};