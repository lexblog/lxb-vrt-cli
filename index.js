// The command line argument parser
const yargs = require('yargs');
// Path module
const { join } = require('path');
// NPM require-dir to get all of the files in a directory
const requireDir = require('require-dir');
// Save the directory name in the global object so it can be referenced
global.appRoot = __dirname;

// A lot of the structure below for dealing with yargs is taken from https://eridem.net/creating-a-cli-tool-in-node-js
// Gets all of the files in the /lib/commands/ directory and looks
// To see which files have modules that have been exported and can be required
// See https://www.npmjs.com/package/require-dir for more details
const commandsFn = requireDir(join(__dirname, 'lib', 'commands'));
// For each module, map them into an array
const commands = Object.keys(commandsFn).map((i) => commandsFn[i]());

// Then for each module in the array, pass the various components of the module to yargs
// so that the commands can be exposed and handled
commands.forEach(cmd => yargs.command(cmd.command, cmd.desc, cmd.builder, cmd.handler));

// Expose all of the commands to help for use
yargs
    .help()
    .argv;